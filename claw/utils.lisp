(cl:defpackage #:libgit2-claw-utils
  (:use :cl)
  (:export #:libgit2-oid-pointer
           #:libgit2-pointer))

(cl:in-package #:libgit2-claw-utils)

(cffi:define-foreign-type libgit2-pointer ()
  ())

(cffi:define-foreign-type libgit2-oid-pointer (libgit2-pointer)
  ())

(cffi:define-parse-method libgit2-pointer (&optional (type :void))
  (if (and (symbolp type) (string-equal type 'oid))
      (make-instance 'libgit2-oid-pointer
                     :actual-type `(:pointer ,type))
      (make-instance 'libgit2-pointer
                     :actual-type `(:pointer ,type))))
