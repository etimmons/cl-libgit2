(uiop:define-package :libgit2-claw
  (:use :cl))

(claw:defwrapper (:libgit2
                  (:system :libgit2-claw)
                  (:headers "git2.h")
                  (:includes :libgit2-includes)
                  (:targets ((:and :x86-64 :linux) "x86_64-pc-linux-gnu")
                            ((:and :arm64 :linux) "aarch64-linux-gnu")
                            ((:and :arm :linux) "arm-linux-gnu")
                            ((:and :x86-64 :windows) "x86_64-pc-windows-msvc")
                            ((:and :x86-64 :darwin) "x86_64-apple-darwin-gnu")
                            ((:and :arm64 :darwin) "aarch64-darwin-gnu"))
                  (:persistent :libgit2-claw-bindings
                   :bindings-path "claw-bindings/"
                   :depends-on (:libgit2-claw-utils))
                  (:include-definitions "^(git|GIT)_\\w+"))
  :in-package :%libgit2-claw-bindings
  :trim-enum-prefix t
  :recognize-bitfields t
  :recognize-strings t
  :inline-functions nil
  :symbolicate-names (:in-pipeline
                      (:by-removing-prefixes "git_" "GIT_"))
  :override-types ((:pointer libgit2-claw-utils:libgit2-pointer)))
