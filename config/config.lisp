;;;; libgit2 configuration
;;;;
;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2-config)

(defvar *init-on-image-restore* nil
  "If non-NIL, LIBGIT2-INIT will be called in UIOP's image restore hook.")

(defvar *init-on-system-load* nil
  "If non-NIL, LIBGIT2-INIT will be called when the system is loaded.")

(defvar *shutdown-on-image-dump* nil
  "If non-NIL, LIBGIT2-SHUTDOWN will be called in UIOP's image dump hook.")
