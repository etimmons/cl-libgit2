;;;; config package
;;;;
;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(uiop:define-package #:libgit2-config
    (:use #:cl)
  (:export #:*init-on-image-restore*
           #:*init-on-system-load*
           #:*shutdown-on-image-dump*))
