;; Generated by :claw at 2021-09-24T15:41:16.918049Z
(asdf:defsystem #:libgit2-claw-bindings
  :description "Bindings generated by libgit2"
  :author "CLAW"
  :license "Public domain"
  :defsystem-depends-on (:trivial-features)
  :depends-on (:uiop :cffi :libgit2-claw-utils)
  :components
  ((:file "claw-bindings/x86_64-pc-linux-gnu" :if-feature
    (:and :x86-64 :linux))
   (:file "claw-bindings/aarch64-linux-gnu" :if-feature
    (:and :arm64 :linux))
   (:file "claw-bindings/arm-linux-gnu" :if-feature
    (:and :arm :linux))
   (:file "claw-bindings/x86_64-pc-windows-msvc" :if-feature
    (:and :x86-64 :windows))
   (:file "claw-bindings/x86_64-apple-darwin-gnu" :if-feature
    (:and :x86-64 :darwin))
   (:file "claw-bindings/aarch64-darwin-gnu" :if-feature
    (:and :arm64 :darwin))))
#-(:or (:and :arm64 :darwin)(:and :x86-64 :darwin)(:and :x86-64 :windows)(:and :arm :linux)(:and :arm64 :linux)(:and :x86-64 :linux))
(warn "Current platform unrecognized or unsupported by libgit2-claw-bindings system")