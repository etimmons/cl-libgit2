;;;; CLAW utils system definition
;;;;
;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(defsystem #:libgit2-claw-utils
  :version (:read-file-form "version.lisp-expr")
  :license "MIT"
  :description "Utils for CLAW bindings."
  :pathname "claw"
  :components ((:file "utils"))
  :depends-on ("cffi" "uiop"))
