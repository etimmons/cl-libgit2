;;;; CLAW generation helper system definition
;;;;
;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(defsystem #:libgit2-claw
  :version (:read-file-form "version.lisp-expr")
  :license "MIT"
  :description "Helper to generate bindings using CLAW."
  :components ((:file "claw/wrapper")
               (:module :libgit2-includes :pathname "libgit2/include/"))
  :depends-on ("cffi" "claw" "libgit2-claw-utils" "uiop"))
