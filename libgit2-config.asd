;;;; config system definition
;;;;
;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(defsystem #:libgit2-config
  :description "Configuration for the libgit2 system."
  :license "MIT"
  :version (:read-file-form "version.lisp-expr")
  :pathname "config"
  :components ((:file "package")
               (:file "config" :depends-on ("package")))
  :depends-on ("uiop"))
