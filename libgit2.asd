;;;; Primary system definition
;;;;
;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(defsystem #:libgit2
  :version (:read-file-form "version.lisp-expr")
  :license "MIT"
  :pathname "src"
  :in-order-to ((test-op (load-op "libgit2/test")))
  :perform (test-op (o c)
                    (unless (eql :passed (uiop:symbol-call
                                          :parachute :status
                                          (uiop:symbol-call :parachute :test :libgit2-test)))
                      (error "Tests failed")))
  :components ((:file "clone" :depends-on ("fetch" "package" "repository" "conditions" "options-structs"))
               (:file "conditions" :depends-on ("package"))
               (:file "credentials" :depends-on ("package" "conditions"))
               (:file "fetch" :depends-on ("remote" "package" "repository" "conditions" "options-structs"))
               (:file "finalizer" :depends-on ("package"))
               (:file "hooks" :depends-on ("package" "library" "libgit2"))
               (:file "libgit2" :depends-on ("package" "conditions"))
               (:file "library" :depends-on ("package"))
               (:file "oid" :depends-on ("package" "libgit2"))
               (:file "options-structs" :depends-on ("package"))
               (:file "package")
               ;;(:file "reference" :depends-on ("package" "repository" "conditions" "options-structs"))
               (:file "remote" :depends-on ("package" "repository" "conditions" "options-structs"))
               (:file "repository" :depends-on ("package" "libgit2")))
  :depends-on ("alexandria" "cffi" "libgit2-claw-bindings" "libgit2-claw-utils"
                            "libgit2-config" "trivial-garbage" "uiop"))

(defsystem #:libgit2/test
  :version (:read-file-form "version.lisp-expr")
  :license "MIT"
  :pathname "test"
  :components ((:file "package")
               (:file "utils" :depends-on ("package"))
               (:file "clone" :depends-on ("package" "utils")))
  :depends-on (#:alexandria #:parachute))
