(in-package :cl-user)

(require "asdf")

(defvar *failed-p* nil)

(unless (null (nth-value 1 (ignore-errors (asdf:test-system :libgit2))))
  (setf *failed-p* t))

(when *failed-p*
  (uiop:quit 1))
