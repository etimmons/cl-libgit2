;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2)

(define-options-struct bind:clone-options)

(defmethod options-struct-populate :after ((type (eql 'bind:clone-options))
                                           data
                                           pointer
                                           payload)
  (setf (cffi:foreign-slot-value pointer type 'bind:remote-cb)
        (cffi:callback %clone-options-remote-create)))

(defgeneric clone-options-remote-create (options repository name url &key &allow-other-keys)
  (:documentation
   "Called while cloning to create a remote. Must return a REMOTE object. All
of the arguments to CLONE are passed as keyword arguments."))

(defmethod clone-options-remote-create ((options options) repository name url &rest args)
  "Default method. Calls CLONE-OPTIONS-REMOTE-INITARGS to get a list of
initargs and then instantiates the remote."
  (let ((initargs (apply #'clone-options-remote-initargs options repository name url args)))
    ;; TODO: Set "remote.%s.mirror" on the repo config. Note that this is
    ;; config for the repo, not remote, but it's prefixed with remote.NAME. So
    ;; need to decide if it should be set through the remote or the repo...
    (apply #'make-instance 'remote initargs)))

(defgeneric clone-options-remote-initargs (options repository name url &key &allow-other-keys)
  (:documentation
   "Called by the default CLONE-OPTIONS-REMOTE-CREATE. Must return a list of
initargs for a REMOTE object. All of the arguments to CLONE are passed as
keyword arguments.")
  (:method-combination append))

(defmethod clone-options-remote-initargs append ((options options) repository name url &key mirrorp)
  "Default behavior. If MIRRORP is non-NIL, the remote is created with a
fetchspec of +refs/*:refs/* and sets the mirror property to true."
  (append
   (list :repository repository
         :name name
         :url url)
   (when mirrorp
     (list :fetchspec "+refs/*:refs/*"))))

(cffi:defcallback %clone-options-remote-create bind:error-code
    ((out :pointer)
     (repo :pointer)
     (name :string)
     (url :string))
  (let ((remote (apply #'clone-options-remote-create *callback-options* repo name url *callback-args*)))
    ;; Do *not* automatically free the remote!
    (setf (finalizep remote) nil)
    (ensure-finalization! remote)
    (setf (cffi:mem-ref out '(:pointer :pointer)) (pointer remote)))
  :ok)

(defgeneric clone-options-barep (options &key &allow-other-keys))

(defmethod clone-options-barep ((options options) &key barep)
  barep)

(defgeneric clone-options-checkout-branch (options &key &allow-other-keys))

(defmethod clone-options-checkout-branch ((options options) &key checkout-branch)
  checkout-branch)

(defgeneric clone (from to &key checkout-branch barep))

(defmethod clone ((from string) (to string)
                  &rest args
                  &key (options (make-instance 'options))
                    checkout-branch barep callbacks
                  &allow-other-keys)
  (declare (ignore barep checkout-branch))
  (let ((*callback-options* options)
        (*callback-args* (list* :from from :to to (a:remove-from-plist args :options))))
    (cffi:with-foreign-object (out :pointer)
      (with-clone-options (opts :bare (apply #'clone-options-barep options args)
                                :checkout-branch (apply #'clone-options-checkout-branch options args)
                                :fetch-opts (:callbacks callbacks))
        (? (bind:clone out from to opts))
        (make-instance 'repository :pointer (cffi:mem-ref out :pointer))))))

(defgeneric barep (repo))

(defmethod barep ((repo repository))
  (bind:repository-is-bare repo))
