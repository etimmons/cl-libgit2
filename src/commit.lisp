(cl:in-package #:libgit2)

(define-backed-object commit ()
  ()
  (:free-function bind:commit-free))

;; (defun commit-from-oid (oid)
;;   (bind:commit-lookup-prefix ))

(defmethod print-object ((commit commit) stream)
  (print-unreadable-object (commit stream :type t :identity t)
    (format stream "~S" (oid-to-str (oid commit)))))

(defmethod oid ((commit commit))
  (bind:commit-id commit))

;; (defun oid-to-str (oid)
;;   (bind:oid-tostr-s oid))

;; (defun oid-from-str (string)
;;   (let ((pointer (cffi:foreign-alloc '(:struct bind:oid)))
;;         (successp nil))
;;     (unwind-protect
;;          (progn
;;            (? (bind:oid-fromstr pointer string))
;;            (setf successp t)
;;            (make-instance 'oid :pointer pointer))
;;       (unless successp
;;         (cffi:foreign-free pointer)))))
