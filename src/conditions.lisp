;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2)

(define-condition git-error (error)
  ((code
    :initarg :code
    :initform nil
    :reader git-error-code)
   (class
    :initarg :class
    :reader git-error-class)
   (message
    :initarg :message
    :reader git-error-message))
  (:report (lambda (c stream)
             (write-string (git-error-message c) stream))))

(defun git-error-from-libgit2 (code)
  (let ((pointer (bind:error-last)))
    (if (cffi:null-pointer-p pointer)
        (make-condition 'git-error
                        :code code)
        (make-condition 'git-error
                        :code code
                        :class (cffi:foreign-enum-keyword
                                'bind:error-t
                                (cffi:foreign-slot-value pointer 'bind:error 'bind:klass))
                        :message (cffi:foreign-slot-value pointer 'bind:error 'bind:message)))))

(defun call-with-git-error (thunk &key countp)
  (let ((raw-result (funcall thunk)))
    (if (and countp (not (minusp raw-result)))
        raw-result
        (let ((result (cffi:foreign-enum-keyword 'bind:error-code raw-result)))
          (if (eql result :ok)
              :ok
              (error (git-error-from-libgit2 result)))))))

(defmacro with-git-error ((&key countp) &body body)
  `(call-with-git-error (lambda () ,@body) :countp ,countp))

(defmacro ? (&body body)
  `(with-git-error (:countp nil) ,@body))

(defmacro ?+ (&body body)
  `(with-git-error (:countp t) ,@body))
