;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2)

(defclass credential-userpass ()
  ((username
    :initarg :username
    :reader username)
   (password
    :initarg :password
    :reader password)))

(defclass credential-ssh-key ()
  ())

(defclass credential-ssh-key-from-agent (credential-ssh-key)
  ((%username
    :initarg :username
    :reader username)))

(defgeneric credential-type (credential)
  (:method ((credential credential-userpass))
    :userpass-plaintext)
  (:method ((credential credential-ssh-key))
    :ssh-key))

(defun default-credentials ()
  (remove nil (list (unless (null (uiop:getenvp "SSH_AUTH_SOCK"))
                      (make-instance 'credential-ssh-key-from-agent)))))

(defgeneric credential-to-libgit (credential pointer)
  (:method ((credential credential-ssh-key-from-agent) pointer)
    (? (bind:credential-ssh-key-from-agent pointer (username credential))))
  (:method ((credential credential-userpass) pointer)
    (? (bind:credential-userpass-plaintext-new pointer (username credential) (password credential)))))
