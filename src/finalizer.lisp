;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2)

(defclass libgit2-finalizer ()
  ((pointer
    :initarg :pointer
    :accessor pointer)
   (free-fun
    :initarg :free-fun
    :accessor free-fun)
   (freedp
    :initform nil
    :accessor freedp))
  (:documentation "A class used to track the finalization state of
LIBGIT2-BACKED-OBJECTs. We do this because we want to have the ability to
cancel finalization (in case the user manually frees the underlying
object). Unfortunately, trivial-garbage's interface provides no way to cancel a
single finalizer for an object and we do not want to step on the user's toes in
case they want to add another finalizer to an object (for whatever reason). So
we simply track in this object if the object has already been freed."))

(defun finalize! (finalizer)
  (unless (freedp finalizer)
    (funcall (free-fun finalizer) (pointer finalizer))
    (setf (freedp finalizer) t)))

(defun make-finalizer-fun (finalizer)
  (lambda ()
    (finalize! finalizer)))
