;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2)

(defun image-restore-hook ()
  "This function is meant to be pushed onto a list of image restore hooks. It
calls LIBGIT2-INIT iff *INIT-ON-IMAGE-RESTORE*."
  (when config:*init-on-image-restore*
    (libgit2-init)))

(defun image-dump-hook ()
  "This function is meant to be pushed onto a list of image dump hooks. It
calls LIBGIT2-SHUTDOWN iff *SHUTDOWN-ON-IMAGE-DUMP*."
  (when config:*shutdown-on-image-dump*
    (libgit2-shutdown)))

(uiop:register-image-restore-hook 'image-restore-hook config:*init-on-system-load*)

(uiop:register-image-dump-hook 'image-dump-hook)
