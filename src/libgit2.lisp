;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2)

(defclass libgit2-backed-object ()
  ((%pointer
    :initarg :pointer
    :accessor pointer
    :documentation "The pointer to the actual C object.")
   (%finalizer
    :accessor finalizer
    :documentation "The LIBGIT2-FINALIZER object.")
   (finalizep
    :initarg :finalizep
    :initform t
    :accessor finalizep
    :documentation "Non-NIL iff we are responsible for freeing the object.")
   (owner
    :accessor owner
    :initform nil
    :initarg :owner
    :documentation "A string reference to the parent object.")
   (owns
    :initform (tg:make-weak-hash-table :weakness :key)
    :accessor owns
    :documentation "Weak pointers to the objects owned by this object."))
  (:documentation
   "An object that corresponds to a libgit2 object C side.

Maintains a (strong) handle on its owner (if there is one) and weak handles on
the (Lisp) objects that it owns. The pointer must be assigned during
initialization."))

(defmethod initialize-instance :after ((obj libgit2-backed-object) &key)
  "Call ENSURE-FINALIZATION!"
  (when (slot-boundp obj '%pointer)
    (ensure-finalization! obj)))

(defmacro define-backed-object (name direct-superclasses direct-slots &rest options)
  "Helper macro to define new subclasses of LIBGIT2-BACKED-OBJECT. Can use
:FREE-FUNCTION option to specify a method on FREE-FUNCTION."
  (let ((free-option (find :free-function options :key #'car)))
    `(progn
       (defclass ,name (,@direct-superclasses libgit2-backed-object)
         ,direct-slots
         ,@(remove free-option options))
       ,@(when free-option
           `((defmethod free-function ((,(gensym) ,name))
               ',(second free-option)))))))

(defgeneric free-function (obj)
  (:documentation "Return a function designator that can be used to free the OBJ."))

(defun ensure-finalization! (obj)
  "Ensure finalizer is registered iff FINALIZEP."
  (when (and (finalizep obj)
             (not (slot-boundp obj '%finalizer)))
    (let ((finalizer (make-instance 'libgit2-finalizer
                                    :pointer (pointer obj)
                                    :free-fun (free-function obj))))
      (setf (finalizer obj) finalizer)
      (tg:finalize obj (make-finalizer-fun finalizer))
      t)))

(defmethod cffi:translate-to-foreign ((value libgit2-backed-object) (type libgit2-claw-utils:libgit2-pointer))
  (let ((pointer (pointer value)))
    (when (null pointer)
      (error "Calling a function with an object that has already been freed."))
    pointer))

(defgeneric free (obj)
  (:documentation "Manually free an object."))

(defmethod free ((obj libgit2-backed-object))
  (let ((pointer (pointer obj))
        (owner (owner obj)))
    (unless (null pointer)
      (unless (finalizep obj)
        (error "Calling FREE on ~S, which has been configured to not be finalized!" obj))
      (finalize! (finalizer obj))
      (funcall (free-function obj) pointer)
      (setf (pointer obj) nil))
    ;; Release handles on owner and owner's handle on us.
    (unless (null owner)
      (remhash obj (owns owner))
      (setf (owner obj) nil))))

(defun call-with-git-object (thunk object)
  (unwind-protect
       (funcall thunk object)
    (free object)))

(defmacro with-git-object ((var &optional (value var)) &body body)
  "Evaluate BODY in a context where VAR is bound to VALUE. After BODY is
finished, FREEs the object that VALUE evaluates to."
  `(call-with-git-object (lambda (,var) ,@body) ,value))

(defmethod (setf pointer) :after (value (obj libgit2-backed-object))
  (ensure-finalization! obj))

(defun libgit2-features ()
  "Return the features libgit2 was compiled with."
  (cffi:foreign-bitfield-symbols 'bind:feature-t (bind:libgit2-features)))

(defun libgit2-version ()
  "Return the libgit2 version as a list of three elements: major, minor, and
patch."
  (cffi:with-foreign-objects ((major :int)
                              (minor :int)
                              (rev :int))
    (bind:libgit2-version major minor rev)
    (list (cffi:mem-ref major :int)
          (cffi:mem-ref minor :int)
          (cffi:mem-ref rev :int))))

(defun libgit2-init ()
  "Initialize libgit2. Returns the number of net initializations."
  (?+ (bind:libgit2-init)))

(defun libgit2-shutdown ()
  "Shutdown libgit2. Returns the number of net initializations."
  (?+ (bind:libgit2-shutdown)))
