;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2)

(cffi:define-foreign-library (libgit2 :canary "git_libgit2_version")
  (t (:default "libgit2")))

(cffi:use-foreign-library libgit2)
