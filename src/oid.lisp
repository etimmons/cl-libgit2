;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2)

(defvar *oids* (tg:make-weak-hash-table :test 'equal :weakness :value)
  "A weak hash table that maps OID strings to OID objects. Used to intern OIDs
so they can safely compared with EQ.")

(define-backed-object oid ()
  ()
  (:free-function cffi:foreign-free)
  (:documentation "An object ID. All object IDs are allocated by the libgit2
caller, so there is no git_ function to free it."))

(defmethod cffi:translate-from-foreign (pointer (type libgit2-claw-utils:libgit2-oid-pointer))
  (oid (bind:oid-tostr-s pointer)))

(defmethod print-object ((oid oid) stream)
  (print-unreadable-object (oid stream :type t :identity t)
    (format stream "~S" (oid-to-str oid))))

(defun oid-to-str (oid)
  (bind:oid-tostr-s oid))

(defgeneric oid (object))

(defmethod oid :around (object)
  "Intern OIDs."
  (let* ((next (call-next-method))
         (str (oid-to-str next)))
    (nth-value 0 (a:ensure-gethash str *oids* next))))

(defmethod oid ((object string))
  "Coerce the string to an OID."
  (let ((pointer (cffi:foreign-alloc '(:struct bind:oid)))
        (successp nil))
    (unwind-protect
         (progn
           (? (bind:oid-fromstr pointer object))
           (setf successp t)
           (make-instance 'oid :pointer pointer))
      (unless successp
        (cffi:foreign-free pointer)))))
