;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2)

(defvar *options-structs* nil)

(defvar *callback-payload*)

(defvar *callback-options*)

(defvar *callback-args*)

(defclass options ()
  ())

(defun options-struct-process-nested (struct args)
  (loop
    :for (key value) :on args :by #'cddr
    :for slot-name := (find-symbol (string key) '#:%libgit2-claw-bindings)
    :for slot-type := (cffi:foreign-slot-type struct slot-name)
    :collect key
    :if (and (ignore-errors (cffi:foreign-slot-names slot-type))
             (listp value))
      :collect `(list ,@(options-struct-process-nested slot-type value))
    :else
      :collect value))

(defun options-struct-make-version (cname)
  (let ((result (find-symbol (concatenate 'string "+" (string cname) (string '-version+))
                             '#:%libgit2-claw-bindings)))
    (when (null result)
      (error "Unable to find version symbol"))
    result))

(defun options-struct-make-init (cname)
  (let ((result (find-symbol (concatenate 'string (string cname) (string '-init))
                             '#:%libgit2-claw-bindings)))
    (when (null result)
      (error "Unable to find init symbol"))
    result))

(defun options-struct-make-with-payloads-name (cname)
  (intern (concatenate 'string (string 'with-) (string cname) (string '-payloads))))

(defun options-struct-make-call-with-payloads-name (cname)
  (intern (concatenate 'string (string 'call-with-) (string cname) (string '-payloads))))

(defun options-struct-make-with-name (cname)
  (intern (concatenate 'string (string 'with-) (string cname))))

(defun options-struct-make-call-with-name (cname)
  (intern (concatenate 'string (string 'call-with-) (string cname))))

(defun options-struct-make-populate-name (cname)
  (intern (concatenate 'string (string 'populate-) (string cname))))

(defun options-struct-make-key-list (slot-names)
  (let ((out nil))
    (dolist (slot-name slot-names)
      (push (intern (string slot-name)) out))
    (nreverse out)))

(defun options-struct-make-helper-slot-filler (ctype pointer-name slot-name callbacks)
  (let* ((var-name (intern (string slot-name)))
         (slot-type (cffi:foreign-slot-type ctype slot-name)))
    (cond
      ((and (ignore-errors (cffi:foreign-slot-names slot-type))
            (member slot-type *options-structs*))
       `(unless (null ,var-name)
          (options-struct-populate ',slot-type ,var-name
                                   (cffi:foreign-slot-pointer ,pointer-name ',ctype ',slot-name)
                                   *callback-payload*)))
      ((member slot-name callbacks :key #'first)
       (destructuring-bind (_ cb-name &optional payload-place)
           (find slot-name callbacks :key #'first)
         (declare (ignore _))
         `(unless (null ,var-name)
            (setf (cffi:foreign-slot-value ,pointer-name ',ctype ',slot-name)
                  (cffi:callback ,cb-name))
            ,@(unless (null payload-place)
                `((setf ,payload-place ,var-name))))))
      ((member slot-type cffi:*built-in-integer-types*)
       `(unless (null ,var-name)
          (setf (cffi:foreign-slot-value ,pointer-name ',ctype ',slot-name)
                (if (eql t ,var-name)
                    1
                    ,var-name))))
      (t
       `(unless (null ,var-name)
          (setf (cffi:foreign-slot-value ,pointer-name ',ctype ',slot-name)
                ,var-name))))))

(defun options-struct-nested-structs (cname)
  (loop :for slot-name :in (cffi:foreign-slot-names cname)
        :for slot-type := (cffi:foreign-slot-type cname slot-name)
        :for slot-is-struct-p := (ignore-errors (cffi:foreign-slot-names slot-type))
        :when slot-is-struct-p
          :collect (cons slot-name slot-type)))

(defun options-struct-make-nested-payloads-calls (nested-structs body)
  (loop :for (_ . slot-type) :in nested-structs
        :when (member slot-type *options-structs*)
          :do (setf body (options-struct-make-nested-payloads-calls
                          (options-struct-nested-structs slot-type)
                          body))
              (setf body (list (options-struct-make-with-payloads-name slot-type) nil body)))
  body)

(defgeneric options-struct-populate (type data pointer payload))

(defmacro define-options-struct (name-and-options &rest options)
  (declare (ignore options))
  (destructuring-bind (cname &key
                               (version (options-struct-make-version cname))
                               (init (options-struct-make-init cname)))
      (if (listp name-and-options)
          name-and-options
          (list name-and-options))
    (let* ((with-name (options-struct-make-with-name cname))
           (call-with-name (options-struct-make-call-with-name cname))
           (slot-names (remove 'bind:version (cffi:foreign-slot-names cname)))
           (key-list (options-struct-make-key-list slot-names))
           (data-name (gensym))
           (pointer-name (gensym))
           (payload-name (gensym)))
      (pushnew cname *options-structs*)
      `(progn
         (defmethod options-struct-populate ((,(gensym) (eql ',cname))
                                             (,data-name list)
                                             ,pointer-name ,payload-name)
           (declare (ignore ,payload-name))
           (destructuring-bind (&key ,@key-list)
               ,data-name
             ,@(loop :for slot-name :in slot-names
                     :for filler := (options-struct-make-helper-slot-filler cname pointer-name slot-name nil)
                     :when filler
                       :collect filler)))

         (defun ,call-with-name (thunk &rest args &key ,@key-list)
           (declare (ignore ,@key-list))
           (let ((*callback-payload* (make-hash-table)))
             (cffi:with-foreign-object (opts ',cname)
               (? (,init opts ,version))
               (options-struct-populate ',cname args opts *callback-payload*)
               (funcall thunk opts))))

         (defmacro ,with-name ((opts &rest args &key ,@key-list) &body body)
           (declare (ignore ,@key-list))
           (list* ',call-with-name (list* 'lambda (list opts) body)
                  (options-struct-process-nested ',cname args)))))))
