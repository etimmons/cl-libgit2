;;;; Package definition
;;;;
;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:defpackage #:libgit2
  (:use #:cl)
  (:local-nicknames (#:a #:alexandria)
                    (#:bind #:%libgit2-claw-bindings)
                    (#:config #:libgit2-config))
  (:export #:barep
           #:clone
           #:credential-userpass
           #:libgit2-features
           #:libgit2-init
           #:libgit2-shutdown
           #:libgit2-version
           #:reference
           #:remote-callbacks
           #:remote-callbacks-credentials))
