;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2)

(define-backed-object reference ()
  ()
  (:free-function bind:reference-free))

(defmethod name ((reference reference))
  (bind:reference-name reference))

(defmethod print-object ((ref reference) stream)
  (print-unreadable-object (ref stream :type t :identity t)
    (format stream "~S"  (name ref))))
