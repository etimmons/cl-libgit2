;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2)

(define-options-struct (bind:remote-callbacks :init bind:remote-init-callbacks))

(define-options-struct (bind:remote-create-options))

(define-backed-object remote ()
  ()
  (:free-function bind:remote-free))

(defmethod initialize-instance :after ((remote remote)
                                       &key url name pointer fetchspec repository
                                         skip-default-fetchspec
                                         skip-instead-of)
  (when (null pointer)
    ;; If the pointer is provided, then we're constructing a reference to an
    ;; already existing remote. If it is not provided, we have to create the
    ;; remote on the C side as well.
    (with-remote-create-options (opts :repository repository
                                      :name name
                                      :fetchspec fetchspec
                                      :flags (append (when skip-default-fetchspec
                                                       (list :default-fetchspec))
                                                     (when skip-instead-of
                                                       (list :insteadof))))
      (cffi:with-foreign-object (out :pointer)
        (bind:remote-create-with-opts out url opts)
        (setf (pointer remote) (cffi:mem-ref out :pointer))))))

(defclass remote-callbacks ()
  ((previous-credentials
    :initform nil
    :accessor remote-callbacks-previous-credentials)))

(defgeneric remote-callbacks-credentials (remote-callbacks
                                          url
                                          username-from-url
                                          allowed-types
                                          previous-results)
  (:method ((remote-callbacks remote-callbacks) url username-from-url allowed-types previous-results)
    nil))

(defmethod options-struct-populate ((type (eql 'bind:remote-callbacks))
                                    (data remote-callbacks)
                                    pointer
                                    payload)
  (setf (cffi:foreign-slot-value pointer type 'bind:credentials)
        (cffi:callback credential-acquire-cb))
  (setf (gethash '%remote-callbacks-data payload) data))

(defclass remote-callbacks-payload ()
  ((credentials
    :initarg :credentials
    :accessor remote-callbacks-payload-credentials)))

(cffi:defcallback credential-acquire-cb bind:error-code
    ((out :pointer)
     (url :string)
     (username-from-url :string)
     (allowed-types bind:credential-t)
     (payload :pointer))
  (declare (ignore payload))
  (let* ((data (gethash '%remote-callbacks-data *callback-payload*))
         (previous-results (remote-callbacks-previous-credentials data))
         (result (remote-callbacks-credentials
                  data url username-from-url allowed-types previous-results)))
    (if (null result)
        1
        (progn
          (push result (remote-callbacks-previous-credentials data))
          (credential-to-libgit result out)
          :ok))))
