;;;; This file is part of cl-libgit2. See README.org and LICENSE for more
;;;; information.

(cl:in-package #:libgit2)

(define-backed-object repository ()
  ()
  (:free-function bind:repository-free))

(defmethod initialize-instance :after ((repo repository) &key location pointer)
  (unless (a:xor location pointer)
    (error "One of LOCATION or POINTER must be specified."))
  (unless (null location)
    (cffi:with-foreign-object (out :pointer)
      (? (bind:repository-open out location))
      (setf (pointer repo) (cffi:mem-ref out :pointer)))))

(defmethod print-object ((repo repository) stream)
  (print-unreadable-object (repo stream :type t :identity t)
    (format stream "~S" (path repo))))

(defgeneric path (repository))

(defmethod path ((repo repository))
  (bind:repository-path repo))

(defgeneric reference (repository name))

(defmethod reference ((repo repository) (name string))
  (cffi:with-foreign-object (out :pointer)
    (? (bind:reference-dwim out repo name))
    (make-instance 'reference
                   :pointer (cffi:mem-ref out :pointer)
                   :owner repo)))

(defgeneric commit (repository name))

(defmethod commit ((repo repository) (name string))
  (let ((oid (oid name)))
    (cffi:with-foreign-object (out :pointer)
      (bind:commit-lookup out repo oid)
      (make-instance 'commit
                     :pointer (cffi:mem-ref out :pointer)
                     :owner repo))))

(defgeneric branches (repo &key filter))

(defmethod branches ((repo repository) &key (filter :local))
  (cffi:with-foreign-object (out :pointer)
    (let (it
          (branches nil))
      (? (bind:branch-iterator-new out repo filter))
      (setf it (cffi:mem-ref out :pointer))
      (unwind-protect
           (loop
             (handler-bind ((git-error #'(lambda (c)
                                           (when (eql :iterover (git-error-code c))
                                             (return branches)))))
               (cffi:with-foreign-object (ref-out :pointer)
                 (cffi:with-foreign-object (type-out :pointer)
                   (? (bind:branch-next ref-out type-out it))
                   (push (make-instance 'reference
                                        :pointer (cffi:mem-ref ref-out :pointer)
                                        :owner repo)
                         branches)))))
        (bind:branch-iterator-free it)))))
