(cl:in-package #:libgit2)

(defun process-nested-structure-opts (struct args)
  (loop
    :for (key value) :on args :by #'cddr
    :for slot-name := (find-symbol (string key) '#:%libgit2-claw-bindings)
    :for slot-type := (cffi:foreign-slot-type struct slot-name)
    :collect key
    :if (ignore-errors (cffi:foreign-slot-names slot-type))
      :collect `(list ,@(process-nested-structure-opts slot-type value))
    :else
      :collect value))

(defun helper-slot-supplied-p (slot-name)
   (intern (concatenate 'string (string slot-name)
                        (string '-supplied-p))))

(defun make-helper-key-list (slot-names slot-options)
  (let ((out nil))
    (dolist (slot-name slot-names)
      (let* ((options (cdr (find slot-name slot-options :key #'car)))
             (default-value (getf options :default)))
        (push (list (intern (string slot-name)) default-value (helper-slot-supplied-p slot-name))
              out)))
    (nreverse out)))

(defun make-helper-slot-filler (ctype pointer-name slot-name slot-options)
  (let* ((var-name (intern (string slot-name)))
         (var-supplied-name (helper-slot-supplied-p var-name))
         (options (cdr (find slot-name slot-options :key #'car :test #'string-equal)))
         (transform (getf options :transform 'identity))
         (slot-type (cffi:foreign-slot-type ctype slot-name)))
    (if (ignore-errors (cffi:foreign-slot-names slot-type))
        (let ((populate-name (intern (concatenate 'string (string 'populate-) (string slot-type)))))
          `(when (and ,var-supplied-name ,var-name)
             (apply #',populate-name (cffi:foreign-slot-pointer ,pointer-name ',ctype ',slot-name)
                    (,transform ,var-name))))
        `(when (and ,var-supplied-name ,var-name)
           (setf (cffi:foreign-slot-value ,pointer-name ',ctype ',slot-name)
                 (,transform ,var-name))))))

(defmacro define-options-struct-helpers-2 (name-and-options ))

(defmacro define-options-struct-helpers ((ctype version init-fun) slot-options)
  (let* ((name (string ctype))
         (populate-name (intern (concatenate 'string (string 'populate-) name)))
         (call-with-name (intern (concatenate 'string (string 'call-with-) name)))
         (with-name (intern (concatenate 'string (string 'with-) name)))
         (slot-names (cffi:foreign-slot-names ctype))
         (key-list (make-helper-key-list slot-names slot-options)))

    `(progn
       (defun ,populate-name (opts &key ,@key-list)
         ,@(loop :for slot-name :in slot-names
                 :collect (make-helper-slot-filler ctype 'opts slot-name slot-options)))

       (defun ,call-with-name (thunk &rest args &key ,@(mapcar #'butlast key-list))
         (declare (ignore ,@(mapcar #'first key-list)))
         (cffi:with-foreign-object (opts ',ctype)
           (? (,init-fun opts ,version))
           (apply #',populate-name opts args)
           (funcall thunk opts)))

       (defmacro ,with-name ((opts &rest args &key ,@(mapcar #'butlast key-list)) &body body)
         (declare (ignore ,@(mapcar #'first key-list)))
         (list* ',call-with-name (list* 'lambda (list opts) body)
                (process-nested-structure-opts ',ctype args))))))
