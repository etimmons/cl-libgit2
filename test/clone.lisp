(in-package #:libgit2-test)

(defclass my-remote-callbacks (git:remote-callbacks)
  ())

(defmethod git:remote-callbacks-credentials ((remote-callbacks my-remote-callbacks)
                                             url
                                             username-from-url
                                             allowed-types
                                             previous-results)
  (make-instance 'git:credential-userpass
                 :username "gitlab+deploy-token-506504"
                 :password "idMBzaZZnVMsx-LsPxCj"))

(define-test clone-repo-test
  (let ((dir (merge-test-dir "test-repo-1/")))
    (maybe-remove-directory dir)
    (git:clone "https://gitlab.com/cl-libgit2/test-repo-1.git"
               (uiop:native-namestring dir))
    (p:is equal "this-is-a-test" (uiop:read-file-line (merge-pathnames "test.txt" dir)))))

;; When these tests are run without SLIME, we seem to routinely get memory
;; corruption. It may even be an issue with the previous test that isn't
;; triggered until a GC runs during the following tests.

;; (define-test clone-repo-bare-test
;;   (let ((dir (merge-test-dir "test-repo-1.bare/")))
;;     (maybe-remove-directory dir)
;;     (let ((repo (git:clone "https://gitlab.com/cl-libgit2/test-repo-1.git"
;;                            (uiop:native-namestring dir)
;;                            :barep t)))
;;       (p:true (git:barep repo)))))

;; (define-test clone-repo-branch-test
;;   (p:skip "Has some issues when compiling from scratch in CI:

;; free(): invalid pointer
;; fatal error encountered in SBCL pid 15130 tid 15144:
;; SIGABRT received."

;;     (let ((dir (merge-test-dir "test-repo-1.main-2/")))
;;       (maybe-remove-directory dir)
;;       (git:clone "https://gitlab.com/cl-libgit2/test-repo-1.git"
;;                  (uiop:native-namestring dir)
;;                  :checkout-branch "main-2")
;;       (p:is equal "this-is-branch-main-2" (uiop:read-file-line (merge-pathnames "test.txt" dir))))))

;; (define-test protected-https-clone-repo-test
;;   (let ((dir (merge-test-dir "test-repo-2/")))
;;     (maybe-remove-directory dir)
;;     (git:clone "https://gitlab.com/cl-libgit2/test-repo-2.git"
;;                (uiop:native-namestring dir)
;;                :callbacks (make-instance 'my-remote-callbacks))
;;     (p:is equal "this-is-a-test" (uiop:read-file-line (merge-pathnames "test.txt" dir)))))
