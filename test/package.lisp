(cl:defpackage #:libgit2-test
  (:use #:cl)
  (:local-nicknames (#:p #:parachute)
                    (#:a #:alexandria)
                    (#:git #:libgit2)))
