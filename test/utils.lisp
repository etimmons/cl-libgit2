(in-package #:libgit2-test)

(defun test-dir ()
  (asdf:system-relative-pathname "libgit2" "test-scratch" :type :directory))

(defun merge-test-dir (subname)
  (merge-pathnames subname (test-dir)))

(defun maybe-remove-directory (dir)
  (let ((pn (uiop:ensure-directory-pathname dir)))
    (when (uiop:directory-exists-p dir)
      (uiop:delete-directory-tree dir :validate (uiop:subpathp pn (test-dir))))))

(p:define-fixture-capture libgit2 (symbol)
  (declare (ignore symbol))
  (git:libgit2-init)
  (values nil t))

(p:define-fixture-restore libgit2 (symbol value)
  (declare (ignore symbol value))
  (git:libgit2-shutdown)
  nil)

(defmacro define-test (name &body arguments-and-body)
  `(p:define-test ,name
     :fix (libgit2)
     ,@arguments-and-body))
